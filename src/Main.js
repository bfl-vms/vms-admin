import React, { Component } from 'react'
import { Switch, Route } from "react-router-dom";
import Landing from './components/landing/Landing';
export class Main extends Component {
   render() {
       return (
           <Switch>
               <>
                   <Route exact path="/" component={Landing} />
                   <Route  path="/landing" component={Landing} />
               </>
           </Switch>
          
       )
   }
}
 
export default Main
