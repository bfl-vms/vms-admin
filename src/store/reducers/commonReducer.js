import ON_ACTION from "../constants/actionType"

const initialState ={
    action : false
 }
 
const commonReducer = (state = initialState, action) => {
    switch(action.type){
        case ON_ACTION:
            return { ...state, action : action.value}
        default :
         return state    
    }
 }
 export default commonReducer